#include "procf.h"
#include <ntstatus.h>
#include <Windows.h>
#include <winternl.h>

 

ProcessEnumerator::ProcessEnumerator() {
	using NtQuerySystemInformationFn = NTSTATUS WINAPI(IN SYSTEM_INFORMATION_CLASS, OUT PVOID, IN ULONG, OUT PULONG);
	const auto NtQuerySystemInformation = reinterpret_cast<NtQuerySystemInformationFn*>(GetProcAddress(GetModuleHandleW(L"ntdll.dll"), "NtQuerySystemInformation"));
	ULONG return_length;
	while (NtQuerySystemInformation(SystemProcessInformation, buffer.get(), static_cast<ULONG>(buffer_len), &return_length) < 0) {
		buffer = std::unique_ptr<uint8_t[]>(new uint8_t[return_length]);
		buffer_len = return_length;
	}
}
bool ProcessEnumerator::next(ProcessEntry& entry) {
	if (next_offset >= buffer_len) {
		return false;
	}
	const auto pi = reinterpret_cast<const SYSTEM_PROCESS_INFORMATION*>(buffer.get() + next_offset);
	next_offset += pi->NextEntryOffset != 0 ? pi->NextEntryOffset : buffer_len - next_offset;

	entry.id = static_cast<uint32_t>((size_t)pi->UniqueProcessId);
	memset(&entry.name, 0, sizeof(entry.name));
	memcpy(&entry.name, pi->ImageName.Buffer, min(pi->ImageName.Length, sizeof(entry.name) - 1));

	return true;
}