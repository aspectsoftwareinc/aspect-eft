#pragma once
#include "unity.h"
#include "procf.h"
#include <vector>
#include <cstdint>
#include <deque>
#include <map>
#include <atomic>

inline uintptr_t unityplayerBase;
inline uintptr_t gomAddr;
inline GlobalObjectManager gom;
inline uintptr_t gameWorldAddr;
inline uintptr_t cameraAddr;
inline uintptr_t localPlayerAddr;

inline bool pressedKeys[500]; // keyboard is 0-256 and mouse is > 256, so let's make the array unreasonably big to avoid overwriting other data
inline std::atomic<bool> running = true;
inline bool mainLoopDone = true;