﻿#include "visuals.h"
#include "Overlay.h"
#include "xorstr.hpp"
#include "PMemHelper.h"
#include <string>
#include "math.hpp"
#include "globals.h"
#include "unity.h"
PRENDER* PRENDER::Instance()
{
	static PRENDER instance;
	return &instance;
}

const int arm_right[] = { Bones::HumanRUpperarm, Bones::HumanRForearm1, Bones::HumanRPalm };
const int arm_left[] = { Bones::HumanLUpperarm, Bones::HumanLForearm1, Bones::HumanLPalm };

const int leg_right[] = { Bones::HumanRThigh1, Bones::HumanRCalf, Bones::HumanRFoot };
const int leg_left[] = { Bones::HumanLThigh1, Bones::HumanLCalf, Bones::HumanLFoot };

void PRENDER::Render()
{

	auto& local_player = EFTData::Instance()->localPlayer;

	float distance;
	float distance2;
	float MaxDrawDistance = 300.f;
	D3DCOLOR color = 0;


	for (auto& extract : EFTData::Instance()->extracts)
	{
		if (!extract.instance)
			continue;

	
		distance2 = local_player.location.Distance(extract.location);

		D2D1_POINT_2F screen_pos;
		if (WorldToScreenv2(extract.location, screen_pos))
		{
			String((int)screen_pos.x, (int)screen_pos.y, D3DCOLOR_ARGB(255, 199, 45, 199), pFont, true, _xor_("%0.2fm").operator const char* (), distance2);
			String((int)screen_pos.x, (int)screen_pos.y + 13, D3DCOLOR_ARGB(199, 45, 236, 199), pFont, true, _xor_("%s").operator const char* (), extract.name);

		}
	}

	for (auto& player : EFTData::Instance()->players)
	{
		if (!player.instance)
			continue;

		if (player.instance == local_player.instance)
			continue;

		//EFTData::Instance()->DrawSkeleton(player.instance);

		distance = local_player.location.Distance(player.location);

		if (distance > MaxDrawDistance)
			continue;

		if (distance <= 150.f)
			color = D3DCOLOR_ARGB(255, 243, 77, 77); //color red, if less than 150m
		else if (distance > 150.f && distance <= 300.f)
			color = D3DCOLOR_ARGB(255, 130, 236, 23); //color yellow, if less than 300m and greater than 150m

		LOCALGAMEWORLD gameworld = memio->read<LOCALGAMEWORLD>(gameWorldAddr);

		Array playerList = memio->read<Array>((uintptr_t)gameworld.m_pPlayerList);

        for (int i = 0; i < playerList.Count; i++) {
            Vector2D head2D, neck2D, pelvis2D;
            char name[256];
            UnityEngineString playerName;
            D3DCOLOR color;
            Player player;
            PlayerProfile profile;
            PlayerInfo info;
            uintptr_t playerAddr = memio->read<uintptr_t>((uintptr_t)playerList.m_pList + (0x20 + (i * 8)));

            if (!playerAddr)
                continue;

            player = memio->read<Player>(playerAddr);

            // Localplayer
            if (player.m_pLocalPlayerChecker) {
                localPlayerAddr = playerAddr;
                continue;
            }


            profile = memio->read<PlayerProfile>((uintptr_t)player.m_pPlayerProfile);
            info = memio->read<PlayerInfo>((uintptr_t)profile.m_PlayerInfo);
            //playerName = process->Read<UnityEngineString>( (uintptr_t) info.m_pPlayerName );

            // Scav check
            if (info.CreationDate > 0) {
                color = D3DCOLOR_ARGB(225, 0, 0, 255);
            }
            else {
                color = D3DCOLOR_ARGB(255, 179, 71, 255);
            }

            Vector3D headPos = Unity::GetBonePosition(playerAddr, Bones::HumanHead);
            Vector3D neckPos = Unity::GetBonePosition(playerAddr, Bones::HumanNeck);
            Vector3D pelvisPos = Unity::GetBonePosition(playerAddr, Bones::HumanPelvis);

            // Needed bones for anything
            if (!Unity::World2Screen(headPos, &head2D)
                || !Unity::World2Screen(neckPos, &neck2D)
                || !Unity::World2Screen(pelvisPos, &pelvis2D)) {
                continue;
            }

            // Draw name/distance
            if (localPlayerAddr) {
                Vector3D localHead = Unity::GetBonePosition(localPlayerAddr, Bones::HumanHead);
                sprintf_s(name, "%0.2fm", playerName.name, localHead.Distance(headPos));
                
                 String(head2D.x, head2D.y, color, pFont,true, name);
            }

            // Draw Spine
            draw_line(head2D.x, head2D.y, neck2D.x, neck2D.y, color);
            draw_line(neck2D.x, neck2D.y, pelvis2D.x, pelvis2D.y, color);

            // Draw right arm
            Vector2D previous = neck2D;
            for (const int bone : arm_right) {
                Vector2D bone2D;
                Vector3D bone3D = Unity::GetBonePosition(playerAddr, bone);

                if (!Unity::World2Screen(bone3D, &bone2D))
                    continue;

                draw_line(previous.x, previous.y, bone2D.x, bone2D.y, color);
                previous = bone2D;
            }

            // Draw left arm
            previous = neck2D;
            for (const int bone : arm_left) {
                Vector2D bone2D;
                Vector3D bone3D = Unity::GetBonePosition(playerAddr, bone);

                if (!Unity::World2Screen(bone3D, &bone2D))
                    continue;

                draw_line(previous.x, previous.y, bone2D.x, bone2D.y, color);
                previous = bone2D;
            }

            // Draw right leg
            previous = pelvis2D;
            for (const int bone : leg_right) {
                Vector2D bone2D;
                Vector3D bone3D = Unity::GetBonePosition(playerAddr, bone);

                if (!Unity::World2Screen(bone3D, &bone2D))
                    continue;
                D2D1_POINT_2F screen_pos;
                    draw_line(previous.x, previous.y, bone2D.x, bone2D.y, color);
                
                
                previous = bone2D;
            }

            // Draw left leg
            previous = pelvis2D;
            for (const int bone : leg_left) {
                Vector2D bone2D;
                Vector3D bone3D = Unity::GetBonePosition(playerAddr, bone);

                if (!Unity::World2Screen(bone3D, &bone2D))
                    continue;


                previous = bone2D;
            }
        }

		D2D1_POINT_2F screen_pos;
		if (WorldToScreenv2(player.location, screen_pos))
		{
			String((int)screen_pos.x, (int)screen_pos.y, color, pFont, true, _xor_("%0.2fm").operator const char* (), distance);

			if(EFTData::Instance()->IsPlayer(player.instance))
			String((int)screen_pos.x, (int)screen_pos.y + 13, D3DCOLOR_ARGB(250, 255, 91, 91), pFont2, true, _xor_("3").operator const char* ());
		}

		if (WorldToScreenv2(player.headPos, screen_pos))
		{
			Circle((int)screen_pos.x, (int)screen_pos.y, 4, 0, 1, true, 32, D3DCOLOR_ARGB(255, 255, 255, 255));
		}
		
	}
}



bool PRENDER::WorldToScreenv2(const FVector& point3D, D2D1_POINT_2F& point2D)
{
	D3DXVECTOR3 _point3D = D3DXVECTOR3(point3D.x, point3D.z, point3D.y);

	auto& matrix = EFTData::Instance()->viewMatrix;

	if (EFTData::Instance()->IsAiming(EFTData::Instance()->localPlayer.instance) && EFTData::Instance()->get_mpcamera(EFTData::Instance()->localPlayer.instance))
	{
    	 matrix = EFTData::Instance()->getoptic_matrix(EFTData::Instance()->localPlayer.instance);
	}

	D3DXVECTOR3 translationVector = D3DXVECTOR3(matrix._41, matrix._42, matrix._43);
	D3DXVECTOR3 up = D3DXVECTOR3(matrix._21, matrix._22, matrix._23);
	D3DXVECTOR3 right = D3DXVECTOR3(matrix._11, matrix._12, matrix._13);

	float w = D3DXVec3Dot(&translationVector, &_point3D) + matrix._44;

	if (w < 0.098f)
		return false;

	float y = D3DXVec3Dot(&up, &_point3D) + matrix._24;
	float x = D3DXVec3Dot(&right, &_point3D) + matrix._14;


	if (EFTData::Instance()->IsAiming(EFTData::Instance()->localPlayer.instance) && EFTData::Instance()->get_mpcamera(EFTData::Instance()->localPlayer.instance))
	{
		uint64_t chain = memio->ReadChain(EFTData::Instance()->offsets.fpsCamera, { 0x30, 0x18 });

			x /= memio->read<float>(chain + 0x12C);

			if(x < 2.f)
			x /= memio->read<float>(chain + 0xAC);

			y /= memio->read<float>(chain + 0x118);

			if(y < 2.f)
			y /= memio->read<float>(chain + 0x98);
	}

	point2D.x = (1920 / 2) * (1.f + x / w);
	point2D.y = (1080 / 2) * (1.f - y / w);

	return true;
}

