#pragma once

#include "tarkov.h"

#include <cstdint>

struct GameWorldWrapper
{
    char pad_0[0x28];
    LOCALGAMEWORLD* localgameworld;
};

struct unity_transform
{
    char pad_0[0x10];
    void* transform;
};

struct game_object_wrapper
{
    char pad_0[0x18];
    void* gameObject;
};

struct mono_object
{
    char pad_0000[0x30];
    game_object_wrapper* pObjectClass;
    char pad_0038[0x10];
    int16_t Unk;
    char pad_004A[6];
    int32_t Layer;
    int16_t Tag;
    char pad_0056[10];
    char* objectname;
};

struct mono_object_wrapper
{
    char pad_0[0x8];
    mono_object_wrapper* next;
    mono_object* object;
};

struct GlobalObjectManager
{
    mono_object_wrapper* taggedLast;
    mono_object_wrapper* taggedFirst;
    mono_object_wrapper* activeLast;
    mono_object_wrapper* activeFirst;
};


namespace Unity
{
    void PrintGOMObjects(bool tagged);
    void PrintPlayerList();
    Vector3D GetBonePosition(uintptr_t playerAddr, int boneID);
    uintptr_t GetObjectPtrByName(const char* objname, bool tagged);
    bool World2Screen(const Vector3D& world, Vector2D* screen);
}


