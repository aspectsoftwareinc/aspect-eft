#pragma once
#include <memory>


// Process entry.
struct ProcessEntry {
	wchar_t name[60];
	uint32_t id;
};
// Enumerates the process list.
class ProcessEnumerator {
public:
	ProcessEnumerator();
	bool next(ProcessEntry& entry);
private:
	std::unique_ptr<uint8_t[]> buffer;
	size_t buffer_len = 0;
	size_t next_offset = 0;
};